<div class="barreHaut">
    <a class="bouton" href="">Rechercher une question</a>
    <a class="bouton" href="index.php?action=create&controller=question">Créer votre question</a>
</div>

<div class="selection">
    <input type="checkbox" name="filtres"/> <label for="filtres">filtres</label>
    <ul>
        <li class="phases">
            <a href="index.php?action=readAll&selection=toutes&controller=question">Toutes</a>
        </li>
        <li class="phases">
            <a href="index.php?action=readAll&selection=ecriture&controller=question">En phase d'écriture</a>
        </li>
        <li class="phases">
            <a href="index.php?action=readAll&selection=vote&controller=question">En phase de vote</a>
        </li>
        <li class="phases">
            <a href="index.php?action=readAll&selection=terminees&controller=question">Terminées</a>
        </li>
    </ul>
</div>
<ul class="questions">
    <?php

    use App\Vote\Model\DataObject\Responsable;

    $date = date("Y-m-d H:i:s");
    foreach ($questions as $question) {
        $calendrier = $question->getCalendrier();
        $idQuestionURL = rawurlencode($question->getId());
        $organisateur = htmlspecialchars($question->getOrganisateur()->getIdentifiant());
        $titreHTML = htmlspecialchars($question->getTitre());
        echo '<p class = "listes">
            <a href= index.php?action=read&controller=question&idQuestion=' .
            $idQuestionURL . '> ' . $titreHTML . ' : </a>
            <a href="">par ' . $organisateur . ' </a >';
        if ($calendrier->getDebutEcriture() > $date){
            if (isset($_SESSION['user']) && $_SESSION['user']['id'] == $organisateur) {
                echo '<a href = index.php?action=update&controller=question&idQuestion=' .
                    $idQuestionURL . ' ><img class="modifier" src = "..\web\images\modifier.png" ></a >

            <a href = index.php?action=delete&controller=question&idQuestion=' .
                    $idQuestionURL . ' ><img class="delete" src = "..\web\images\delete.png" ></a >';
            }
            if (isset($_SESSION['user']) && Responsable::estResponsable($question, $_SESSION['user']['id'])) {
                if ($calendrier->getDebutEcriture() <= $date && $calendrier->getFinEcriture() >= $date) {
                    echo '<a href = index.php?action=create&controller=proposition&idQuestion=' . $idQuestionURL . '>Créer une proposition</a>';
                }
            }
        }

        echo '<a href = index.php?action=readAll&controller=proposition&idQuestion=' . $idQuestionURL . ' >Liste des propositions</a>';
        echo '</p>';

    }
    ?>
</ul>
