<?php
use App\Vote\Config\FormConfig as FormConfig;
if ((isset($_POST["keyword"]) && isset($_POST["row"]))) {
    $keyword = htmlspecialchars($_POST["keyword"]);
    $row = htmlspecialchars($_POST["row"]);
} else {
    $keyword = "";
    $row = "";
}
?>

<form method="post" >
    <fieldset>
        <legend>Selectionner des <?php echo $_SESSION[FormConfig::$arr]['type'] ?> :</legend>
        <p>
            <input type="text" placeholder="" name="keyword" id="motclef" value ="<?php echo $keyword ?>" required/>
            <input type ="hidden" name = "row" value = "nom" />
            <input type="image" alt = "Submit" src="../web/images/search.png" value="Envoyer" class = "search"/>
        </p>
    </fieldset>
</form>